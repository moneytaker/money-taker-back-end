const Database = require('../../database/database');

const BasicService = {
    insert: async (table, obj) => {
        let values = Object.keys(obj).map((key) => `'${obj[key]}'`).join(',');
        let query = `INSERT INTO money_taker.${table}(${Object.keys(obj)}) VALUES (${values})`;
        await Database.asyncQuery(query);
    },
    update: async (table, idColumn, id, obj) => {
        let values = Object.keys(obj).map((key) => `${key} = '${obj[key]}'`).join(',');
        let query = `UPDATE money_taker.${table} SET ${values} WHERE ${idColumn}=${id}`;
        await Database.asyncQuery(query);
    },
    delete: async (table, idColumn, id) => await Database.asyncQuery(`DELETE FROM money_taker.${table} WHERE ${idColumn} = ${id}`),
    select: async (table, idColumn, id) => (await Database.asyncQuery(`SELECT * FROM money_taker.${table} WHERE ${idColumn} = ${id}`)).rows[0],
    selectAll: async (table) => (await Database.asyncQuery(`SELECT * FROM money_taker.${table}`)).rows,
    selectAllByDates: async (id, startDate, endDate) => (await Database.asyncQuery(`SELECT * FROM money_taker.transactions where account_from_id = ${id} and created_date>'${startDate}' and created_date<'${endDate}'`)).rows,
};

module.exports = BasicService;
