const readXlsxFile = require('read-excel-file/node');
const BasicService = require("../../public/services/BasicService");

exports.uploadFile = (req, res) => {

    try {
        let filePath = global.__basedir + "/uploads/" + req.file.filename;

        readXlsxFile(filePath, {getSheets: true}).then(sheets => {

            sheets.forEach(sheet => {
                    readXlsxFile(filePath, {sheet: sheet.name}).then(async (rows) => {
                        // Remove Header ROW

                        const attributes = rows[0];
                        rows.shift();

                        let length = rows.length;
                        for (let i = 0; i < length; i++) {
                            let itemToSave = {};

                            attributes.forEach((x, index) => {
                                itemToSave[x] = rows[i][index];
                            });
                            await BasicService.insert(sheet.name, itemToSave);
                        }
                    }).catch(x => console.log(x))
                }
            );
            res.json({
                status: "processing",
                filename: req.file.originalname,
                message: "File is loading"
            });
        });
    } catch (error) {
        const result = {
            status: "fail",
            filename: req.file.originalname,
            message: "Upload Error! message = " + error.message
        }
        res.json(result);
    }
}
