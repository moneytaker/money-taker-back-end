const {Pool} = require('pg')
const {pgProps} = require('../config');

const Database = {
    asyncQuery: async function query(query, args) {
        const connection = new Pool({
            host: pgProps.host,
            user: pgProps.user,
            password: pgProps.password,
            database: pgProps.database,
            port: 5432,
            ssl: {
                rejectUnauthorized: false
            }
        });
        return await connection.query(query, args);
    }
};

module.exports = Database;
