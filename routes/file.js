let express = require('express');
let router = express.Router();
let upload = require('../public/common/multer');

const excelWorker = require('../public/common/excel.js');

let path = global.__basedir   + '/views/';

router.get('/', (req,res) => {
    console.log("__basedir" + global.__basedir );
    res.sendFile(path + "index.html");
});

router.post('/', upload.single("file"), excelWorker.uploadFile);


module.exports = router;
