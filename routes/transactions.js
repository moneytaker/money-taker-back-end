const express = require('express');
const BasicService = require("../public/services/BasicService");
const router = express.Router();

router.get('/', async ({query}, res) => {
        try {
            let id = query.id;
            if (id) {
                const response = await BasicService.select('transactions', 'id', id);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'ID is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

router.get('/all', async ({query}, res) => {
    try {
        const response = await BasicService.selectAll('transactions');
        await res.status(200).json(response);

    } catch (error) {
        await res.status(500).json({message: error});
    }
});

router.post('/', async ({body}, res) => {
        try {
            if (body) {
                const response = await BasicService.insert('transactions', body);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'value is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

router.put('/', async ({body}, res) => {
        try {
            if (body) {
                const response = await BasicService.update('transactions', body);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'value is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

router.delete('/', async ({body}, res) => {
        try {
            if (body.id) {
                const response = await BasicService.delete('transactions', body);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'ID is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

router.post('/simple-transaction', async ({body}, res) => {
        try {
            if (!body) {
                await res.status(400).json({message: 'body is missing'});
                return;
            }

            const accountSourceOfMoney = await BasicService.select('accounts', 'id', +body.accountFromId);
            const accountTargetOfMoney = await BasicService.select('accounts', 'id', +body.accountToId);
            const govermentAccount = await BasicService.select('accounts', 'id', 1);

            const amountToMove = body.amountToMove;
            let amountOfTaxes = 0;

            if (!accountSourceOfMoney) {
                await res.status(400).json({message: 'Source money account doesn\'t exist'});
                return;
            }

            if (!accountTargetOfMoney) {
                await res.status(400).json({message: 'Target money account doesn\'t exist'});
                return;
            }

            if (accountSourceOfMoney.currency_id !== accountTargetOfMoney.currency_id) {
                await res.status(400).json({message: 'The accounts aren\'t of the same currency'});
                return;
            }

            const response = await BasicService.selectAll('taxes');
            response.forEach(tax => amountOfTaxes += amountToMove * tax.porcentage / 100);

            if (+accountSourceOfMoney.balance_available < +amountToMove + amountOfTaxes) {
                await res.status(400).json({message: 'Not enought money in the account'});
                return;
            }

            const transactionToProcess = {
                account_from_id: accountSourceOfMoney.id,
                account_to_id: accountTargetOfMoney.id,
                currency_type_id: accountSourceOfMoney.currency_id,
                transaction_type: 2, //Transferencia
                amount: amountToMove
            };
            await BasicService.insert('transactions', transactionToProcess);
            transactionToProcess.account_to_id = 1; // goverment id
            transactionToProcess.transaction_type = 1; // taxes
            transactionToProcess.amount = amountOfTaxes;
            await BasicService.insert('transactions', transactionToProcess);

            accountSourceOfMoney.balance_available = +accountSourceOfMoney.balance_available - amountToMove + amountOfTaxes;
            await BasicService.update('accounts', 'id', accountSourceOfMoney.id, accountSourceOfMoney);

            accountTargetOfMoney.balance_available = +accountTargetOfMoney.balance_available + (+amountToMove);
            await BasicService.update('accounts', 'id', accountTargetOfMoney.id, accountTargetOfMoney);

            govermentAccount.balance_available = +govermentAccount.balance_available + amountOfTaxes;
            await BasicService.update('accounts', 'id', govermentAccount.id, govermentAccount);

            await res.status(200).json({message: 'success'});
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);


router.post('/transactions-by-date', async ({body}, res) => {
        try {
            if (!body) {
                await res.status(400).json({message: 'body is missing'});
                return;
            }

            let transactions = await BasicService.selectAllByDates(body.accountId, body.startDate, body.endDate);
            await res.status(200).json(transactions);

        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);
module.exports = router;
