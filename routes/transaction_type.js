const express = require('express');
const BasicService = require("../public/services/BasicService");
const router = express.Router();

router.get('/', async ({query}, res) => {
        try {
            let id = query.id;
            if (id) {
                const response = await BasicService.select('transaction_type', 'id', id);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'ID is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

router.get('/all', async ({query}, res) => {
    try {
        const response = await BasicService.selectAll('transaction_type');
        await res.status(200).json(response);

    } catch (error) {
        await res.status(500).json({message: error});
    }
});

router.post('/', async ({body}, res) => {
        try {
            if (body) {
                const response = await BasicService.insert('transaction_type', body);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'value is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

router.put('/', async ({body}, res) => {
        try {
            if (body) {
                const response = await BasicService.update('transaction_type', body);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'value is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

router.delete('/', async ({body}, res) => {
        try {
            if (body.id) {
                const response = await BasicService.delete('transaction_type', body);
                await res.status(200).json(response);
            } else {
                await res.status(400).json({message: 'ID is missing'});
            }
        } catch (error) {
            await res.status(500).json({message: error});
        }
    }
);

module.exports = router;
