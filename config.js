// config.js
const dotenv = require('dotenv');
dotenv.config({path: __dirname + '/.env'})
module.exports = {
    pgProps: {
        host: process.env.PG_HOST,
        user: process.env.PG_USER,
        password: process.env.PG_PASSWORD,
        database: process.env.PG_DATABASE
    }
};
