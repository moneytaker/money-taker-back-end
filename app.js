const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');

const indexRouter = require('./routes/index');
const account_typeRouter = require('./routes/account_type');
const accountRouter = require('./routes/accounts');
const currencyRouter = require('./routes/currency');
const currencyTypeRouter = require('./routes/currencyType');
const rolesRouter = require('./routes/roles');
const taxesRouter = require('./routes/taxes');
const transaction_taxesRouter = require('./routes/transaction_taxes');
const transaction_typeRouter = require('./routes/transaction_type');
const transactionsRouter = require('./routes/transactions');
const usersRouter = require('./routes/users');
const fileRouter = require('./routes/file');

const app = express();

global.__basedir = __dirname

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

indexRouter.all('*', cors());
account_typeRouter.all('*', cors());
accountRouter.all('*', cors());
currencyRouter.all('*', cors());
currencyTypeRouter.all('*', cors());
rolesRouter.all('*', cors());
taxesRouter.all('*', cors());
transaction_taxesRouter.all('*', cors());
transaction_typeRouter.all('*', cors());
transactionsRouter.all('*', cors());
usersRouter.all('*', cors());
fileRouter.all('*', cors());

app.use('/', indexRouter);
app.use('/account_type', account_typeRouter);
app.use('/account', accountRouter);
app.use('/currency', currencyRouter);
app.use('/currencyType', currencyTypeRouter);
app.use('/roles', rolesRouter);
app.use('/taxes', taxesRouter);
app.use('/transaction_taxes', transaction_taxesRouter);
app.use('/transaction_type', transaction_typeRouter);
app.use('/transactions', transactionsRouter);
app.use('/users', usersRouter);
app.use('/file', fileRouter);

app.use(cors());
app.options('*', cors());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
